import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Grid } from '@material-ui/core';

const Header = props => {
  const { branding } = props;
  return (
    // <nav className="navbar navbar-expand-sm navbar-dark bg-info mb-3 py-0">
    //   <div className="container">
    //     <a href="/" className="navbar-brand">
    //       {branding}
    //     </a>
    //     <div>
    //       <ul className="navbar-nav mr-auto">
    //         <li className="nav-item">
    //           <Link to="/" className="nav-link">
    //             <i className="fas fa-home" /> Home
    //           </Link>
    //         </li>
    //         <li className="nav-item">
    //           <Link to="/contact/add" className="nav-link">
    //             <i className="fas fa-plus" /> Add
    //           </Link>
    //         </li>
    //         <li className="nav-item">
    //           <Link to="/about" className="nav-link">
    //             <i className="fas fa-question" /> About
    //           </Link>
    //         </li>
    //       </ul>
    //     </div>
    //   </div>
    // </nav>
    <Grid container>

      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          
            
            <Typography variant="h6">
            <a href="/" className="navbar-brand" style={{color:'white',textDecoration:'none'}}>
          {branding}
            </a>
            </Typography>
       
          
          <Link to="/" className="nav-link" style={{ color: 'white', marginLeft: '2%', textDecoration: 'none'}}>
            <Typography ><i className="fas fa-home" /> Home</Typography>
              </Link>
          <Link to="/contact/add" className="nav-link" style={{ color: 'white', marginLeft: '2%', textDecoration: 'none' }}>
            <Typography > <i className="fas fa-plus" /> Add</Typography>
               </Link>
          <Link to="/about" className="nav-link" style={{ color: 'white', marginLeft: '2%', textDecoration: 'none' }}>
            <Typography >
               <i className="fas fa-question" /> About
               </Typography>
            </Link>
          <Link to="/task2" className="nav-link" style={{ color: 'white', marginLeft: '60%', textDecoration: 'none' }}>
            <Typography >
              <i className="fas fa-question" /> Task2
               </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </Grid>
  );
};

Header.defaultProps = {
  branding: 'My App'
};

Header.propTypes = {
  branding: PropTypes.string.isRequired
};

export default Header;
