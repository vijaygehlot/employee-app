import { GET_CONTACTS, DELETE_CONTACT, ADD_CONTACT, GET_CONTACT, UPDATE_CONTACT } from './types';
import axios from 'axios';

export const getContacts = () => async dispatch => {
  const res = await axios.get('https://jsonplaceholder.typicode.com/users');
  // alert('called1')
  dispatch({
    type: GET_CONTACTS,
    payload: res.data
  });
};

export const getContact = (id) => async dispatch => {
  const res = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`);
  alert('called2')
  dispatch({
    type: GET_CONTACT,
    payload: res.data
  });
};

export const deleteContact = id => async dispatch => {
  try {
    await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`).then(res=>{
      
    dispatch({
   type: DELETE_CONTACT,
    payload: id,
    
    });
   
    })
    
  } catch(e) {
    dispatch({
      type: DELETE_CONTACT,
      payload: id
    });
  }
};

export const addContact = contact => async dispatch => {
  const res = await axios.post('https://jsonplaceholder.typicode.com/users', contact).then(res=>{
    
    dispatch({
      type: ADD_CONTACT,
      payload: res.data
    });
   
  })
  
};

export const updateContact = contact => async dispatch => {
  const res = await axios.put(`https://jsonplaceholder.typicode.com/users/${contact.id}`, contact).then(res=>{
    alert('Updated')
   dispatch({
     type: UPDATE_CONTACT,
     payload: res.data
   }); 
  })
  
};